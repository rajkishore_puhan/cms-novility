import { TestBed, inject } from '@angular/core/testing';

import { ViewreservationService } from './view-reservation.service';

describe('ViewreservationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViewreservationService]
    });
  });

  it('should be created', inject([ViewreservationService], (service: ViewreservationService) => {
    expect(service).toBeTruthy();
  }));
});
