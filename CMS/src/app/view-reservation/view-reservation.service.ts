import { Injectable } from '@angular/core';
import { HttpUtilService } from '../util-service/http-util.service';
import { AppUrl } from '../constant/AppUrl';
@Injectable()
export class ViewreservationService {

  constructor(private httpService: HttpUtilService) { }
  getReservations(): any {
    return this.httpService.doGet(AppUrl.RESERVATIONS, {});
  }
  getCars(): any {
    return this.httpService.doGet(AppUrl.CARS, {});
  }
}
