import { Component, OnInit, ViewChild } from '@angular/core';
import { ViewreservationService } from './view-reservation.service';
import { EditReservationComponent } from '../edit-reservation/edit-reservation.component';
import { AddReservationComponent } from '../add-reservation/add-reservation.component';
@Component({
  selector: 'app-root',
  templateUrl: './view-reservation.component.html',
  styleUrls: ['./view-reservation.component.scss'],
  providers: [ViewreservationService]
})
export class ViewReservationComponent implements OnInit {
  title = 'app';
  cols: any= [];
  record: any= [];
  today: any= new Date( ).setHours(0, 0, 0, 0);

  @ViewChild(EditReservationComponent) editObj: EditReservationComponent;
  @ViewChild(AddReservationComponent) addObj: AddReservationComponent;
  constructor(private viewreservationService: ViewreservationService) { }

  openAddPopup() {
    this.addObj.openAdd();
  }
  openEditPopup(rowObj) {
    this.editObj.openEdit(rowObj);
  }
  goPrevious() {
    for (let index = 0; index < this.cols.length; index++) {
      let  thisDate = new Date(this.cols[index].headers);
      this.cols[index].headers = new Date(thisDate.setDate(thisDate.getDate() - 1));
    }
  }
  goNext() {
    for (let index = 0; index < this.cols.length; index++) {
      let thisDate = new Date(this.cols[index].headers);
      this.cols[index].headers = new Date(thisDate.setDate(thisDate.getDate() + 1));
    }
  }
  ngOnInit() {
    this.viewreservationService.getReservations().subscribe(data => {
      const reservations = data;
      this.viewreservationService.getCars().subscribe(data => {
        let cars = data;
        cars.forEach(function (car, index, carList) {
          reservations.forEach(function (reservation, index2, reservationList) {
            if (reservation.carId === car.id) {
              reservationList[index2].carName = car.carType;
              reservationList[index2].from = new Date (new Date(reservationList[index2].from).setHours(0, 0, 0, 0));
              reservationList[index2].to = new Date(new Date(reservationList[index2].to).setHours(0, 0, 0, 0));
            }
          });
        });
        this.record = reservations;

      });
    });

    // tslint:disable-next-line:prefer-const
    let futureDate = new Date(this.today);
    // futureDate.setHours(23,59,59,999);
    for (let index = 0; index < 12; index++) {
      // tslint:disable-next-line:prefer-const
      let obj = {};
      obj['headers'] = new Date(futureDate.setDate(futureDate.getDate() + 1));
      obj['fields'] = {
        carName: 'carName',
        from: 'from',
        to: 'to',
        id: 'id',
        client: 'clientName',
      };
      this.cols.push(obj);
    }
  }
}
