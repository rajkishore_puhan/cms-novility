import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {InputTextModule} from 'primeng/inputtext';
import {CalendarModule} from 'primeng/calendar';
import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';

import { ViewReservationComponent } from './view-reservation/view-reservation.component';
import { AddReservationComponent } from './add-reservation/add-reservation.component';

import { HttpUtilService } from './util-service/http-util.service';
import { EditReservationComponent } from './edit-reservation/edit-reservation.component';

@NgModule({
  declarations: [
    ViewReservationComponent,
    AddReservationComponent,
    EditReservationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    InputTextModule,
    CalendarModule,
    TableModule,
    DialogModule
  ],
  providers: [HttpUtilService],
  bootstrap: [ViewReservationComponent]
})
export class AppModule { }
