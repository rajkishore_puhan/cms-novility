import { TestBed, inject } from '@angular/core/testing';

import { AddReservationService } from './add-reservation.service';

describe('AddReservationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddReservationService]
    });
  });

  it('should be created', inject([AddReservationService], (service: AddReservationService) => {
    expect(service).toBeTruthy();
  }));
});
