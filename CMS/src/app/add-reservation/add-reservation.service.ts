import { Injectable } from '@angular/core';
import { HttpUtilService } from '../util-service/http-util.service';
import { error } from 'util';
import { AppUrl } from '../constant/AppUrl';

@Injectable()
export class AddReservationService {
  private requestObject: any = {};
  constructor(private httpService: HttpUtilService) {}
  saveReservation(obj): any {
    this.setRequestObject(obj);
    return this.httpService.doPost(AppUrl.RESERVATIONS , this.requestObject);
  }
  getCars(): any {
    return this.httpService.doGet(AppUrl.CARS , {});
  }
  private setRequestObject(obj) {
    this.requestObject.carId = obj.car.id;
    this.requestObject.from = obj.fromDate;
    this.requestObject.to = obj.toDate;
    this.requestObject.clientName = obj.client;
  }
}
