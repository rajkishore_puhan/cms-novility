import { Component, OnInit } from '@angular/core';
import { AddReservationService } from './add-reservation.service';
import { Dropdown } from 'primeng/primeng';
@Component({
  selector: 'app-add-reservation',
  templateUrl: './add-reservation.component.html',
  styleUrls: ['./add-reservation.component.scss'],
  providers: [AddReservationService]
})
export class AddReservationComponent implements OnInit {
  reservation: any = {};
  cars: any = [];
  displayAdd: Boolean = false;
  constructor(private addReservationService: AddReservationService) {}


  saveReservation() {
    alert('Done');
    this.addReservationService.saveReservation(this.reservation).subscribe(
      (success) => {
        alert('Done');
        this.displayAdd = false;
      },
      // tslint:disable-next-line:no-shadowed-variable
      (error) => {
        alert('some Error');
      }
    );
  }
  closeAdd() {
    this.displayAdd = false;
  }
  openAdd() {
    this.displayAdd = true;
  }
  ngOnInit() {
    this.addReservationService.getCars().subscribe(data => {
      this.cars = data;
    });
  }

}
