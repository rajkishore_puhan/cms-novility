import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { AppUrl } from '../constant/AppUrl';
@Injectable()
export class HttpUtilService {

  constructor(private http: Http) { }
  doGet(url: string, requestObj: any) {
    const httpUrl = AppUrl.API_ENDPOINT + url;
    return this.http.get(httpUrl).map(res => res.json());
  }
  doPost(url: string, requestObj: any) {
    const httpUrl = AppUrl.API_ENDPOINT + url;
    return this.http.post(httpUrl, requestObj).map((res: any) => {
      const response: any = JSON.parse(res._body);
        if (response.success === 0) {
          throw Observable.throw(response);
        } else if (response.success === 1) {
          return response;
        }
    });
  }
  doPut(url: string, requestObj: any) {
    const httpUrl = AppUrl.API_ENDPOINT + url;
    return this.http.put(httpUrl, requestObj).map((res: any) => {
      const response: any = JSON.parse(res._body);
        if (response.success === 0) {
          throw Observable.throw(response);
        } else if (response.success === 1) {
          return response;
        }
    });
  }
  doDelete(url: string) {
    const httpUrl = AppUrl.API_ENDPOINT + url;
    return this.http.delete(httpUrl).map((res: any) => {
      const response: any = JSON.parse(res._body);
        if (response.success === 0) {
          throw Observable.throw(response);
        } else if (response.success === 1) {
          return response;
        }
    });
  }
}
