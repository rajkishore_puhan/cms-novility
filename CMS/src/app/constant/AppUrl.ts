export class AppUrl {
    public static API_ENDPOINT = 'http://external.novility.com:9050/car-rental/api/';
    public static RESERVATIONS = 'Reservations';
    public static CARS = 'Cars';
}
