import { Component} from '@angular/core';
import { EditReservationService } from './edit-reservation.service';

@Component({
  selector: 'app-edit-reservation',
  templateUrl: './edit-reservation.component.html',
  styleUrls: ['./edit-reservation.component.scss'],
  providers : [EditReservationService]
})
export class EditReservationComponent {

  constructor(private editReservationService: EditReservationService) {}
  editreservation: any = {
    car: {}
  };
  cars: any = [];
  // tslint:disable-next-line:no-inferrable-types
  displayEdit: boolean = false;
  saveReservation() {
    this.editReservationService.saveReservation(this.editreservation).subscribe(
      (success) => {
        this.displayEdit = false;
        alert('Done');
      },
      // tslint:disable-next-line:no-shadowed-variable
      (error) => {
        this.displayEdit = false;
        alert('some Error');
      }
    );
  }
  deleteReservation() {
    this.editReservationService.deleteReservation(this.editreservation).subscribe(
      (success) => {
        this.displayEdit = false;
        alert('Done');
      },
      // tslint:disable-next-line:no-shadowed-variable
      (error) => {
        this.displayEdit = false;
        alert('some Error');
      }
    );
  }
  openEdit(rowObj) {
    console.log(rowObj);
    this.editReservationService.getCars().subscribe(data => {
      this.cars = data;
      this.editreservation.car = this.cars.filter(function( obj ) {
        return obj.id === rowObj.carId;
      })[0];
    });
    this.editreservation.fromDate = rowObj.from;
    this.editreservation.toDate = rowObj.to;
    this.editreservation.client = rowObj.clientName;
    this.editreservation.id = rowObj.id;
    this.displayEdit = true;
  }
  closeEdit() {
    this.displayEdit = false;
  }
}
