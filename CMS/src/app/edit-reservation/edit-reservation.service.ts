import { Injectable } from '@angular/core';
import { HttpUtilService } from '../util-service/http-util.service';
import { AppUrl } from '../constant/AppUrl';

@Injectable()
export class EditReservationService {

  private requestObject: any = {};
  constructor(private httpService: HttpUtilService) {}
  saveReservation(obj) {
    this.setRequestObject(obj);
    // tslint:disable-next-line:prefer-const
    let url = AppUrl.RESERVATIONS +'/' + obj.id;
    return this.httpService.doPut(url, this.requestObject);
  }
  getCars(): any {
    return this.httpService.doGet(AppUrl.CARS, {});
  }
  deleteReservation(obj) {
    // tslint:disable-next-line:prefer-const
    let url = AppUrl.RESERVATIONS + '/' + obj.id;
    return this.httpService.doDelete(url);
  }
  private setRequestObject(obj) {
    this.requestObject.carId = obj.car.id;
    this.requestObject.from = obj.fromDate;
    this.requestObject.to = obj.toDate;
    this.requestObject.clientName = obj.client;
  }

}
