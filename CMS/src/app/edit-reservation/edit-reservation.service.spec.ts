import { TestBed, inject } from '@angular/core/testing';

import { EditReservationService } from './edit-reservation.service';

describe('EditReservationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EditReservationService]
    });
  });

  it('should be created', inject([EditReservationService], (service: EditReservationService) => {
    expect(service).toBeTruthy();
  }));
});
